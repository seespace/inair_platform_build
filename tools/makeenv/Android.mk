# 
# Copyright 2008 The Android Open Source Project
#
# make spi alignment tool
#

LOCAL_PATH:= $(call my-dir)
include $(CLEAR_VARS)

LOCAL_SRC_FILES := makeenv.c

LOCAL_MODULE := mkenv

include $(BUILD_HOST_EXECUTABLE)

